<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="C" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>***JSP MAGIC PAGE***</title>
    <link href="css/style.css" rel="stylesheet" type="text/css">
</head>

<% Object param = session.getAttribute("allRegionNames");
    String str = String.valueOf(param);
    if (param == null){
        str = "Please submit button";
    }
%>

<body>
<div>
    <form method="post" action="regionList">
        <input type="submit" value="Get Armenian Regions"> <br>
        <h4>All Armenian regions: <c:out value="<%= str%>"/></h4>
     </form>
</div>

<div id="footer">
    <p>&copy; 2015 Artur Babayan</p>

    <p>Contact info:<a href="mailto:arturarmbabayan@gmail.com">arturarmbabayan@gmail.com</a></p>
</div>
</body>
</html>