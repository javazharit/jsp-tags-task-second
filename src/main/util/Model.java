package main.util;

import java.util.ArrayList;
import java.util.List;

public class Model {
    private static Model instance = new Model();

    private Model() {
    }

    public static Model get() {
        return instance;
    }

    public List<RegionsNamesEnum> getAllRegionsNames() {
        List<RegionsNamesEnum> allRegionsList = new ArrayList<>();

        RegionsNamesEnum regionEnumArray[] = RegionsNamesEnum.values();
        for (RegionsNamesEnum regionName : regionEnumArray) {
            allRegionsList.add(regionName);
        }
        return allRegionsList;
    }
}
