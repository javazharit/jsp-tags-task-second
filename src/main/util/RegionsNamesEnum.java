package main.util;

public enum RegionsNamesEnum {
    ARAGATSOTN, ARARAT, ARMAVIR, GEGHARKUNIK, KOTAYK, LORI, SHIRAK, SYUNIK, TAVUSH, YEREVAN;

    public static Enum getRandom() {
        return values()[(int) Math.random() * values().length];
    }
}
